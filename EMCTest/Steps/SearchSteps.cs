﻿using EMCTest.Pages;
using EuroMoneyConferences.Base;
using EuroMoneyConferences.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using SpecFlow.Assist.Dynamic;

namespace EMCTest.Steps
{
    [Binding]
    public class SearchSteps : BaseStep
    {

        [Given(@"I have navigated to the application")]
        public void GivenIHaveNavigatedToTheApplication()
        {
            // driver.Navigate().GoToUrl(Settings.AUT);
            NaviateSite();
            CurrentPage = GetInstance<GooglePage>();
            GooglePage page = new GooglePage();
            page.search.Click();
			var bla = new DynamicStepArgumentTransformations();
			
			


        }

        //[Given(@"I see application opened")]
        //public void GivenISeeApplicationOpened()
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Then(@"I click login link")]
        //public void ThenIClickLoginLink()
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[When(@"I enter UserName and Password")]
        //public void WhenIEnterUserNameAndPassword(Table table)
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Then(@"I click login button")]
        //public void ThenIClickLoginButton()
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Then(@"I should see the username with hello")]
        //public void ThenIShouldSeeTheUsernameWithHello()
        //{
        //    ScenarioContext.Current.Pending();
        //}

        //[Then(@"I click logout")]
        //public void ThenIClickLogout()
        //{
        //    ScenarioContext.Current.Pending();
        //}

    }
}
