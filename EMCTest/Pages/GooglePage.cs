﻿using EuroMoneyConferences.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMCTest.Pages
{
    class GooglePage : BasePage
    {
        

        [FindsBy(How = How.XPath, Using = "//input[@value='Из прве руке']")]
        public IWebElement search { get; set; }

    }
}
