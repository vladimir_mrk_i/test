﻿using EuroMoneyConferences.Base;
using TechTalk.SpecFlow;
using EuroMoneyConferences.Helpers;
using EuroMoneyConferences.Config;

namespace EMCTest
{

    [Binding]
    public class HookInitialize : TestInitializeHook
    {
        public  HookInitialize() : base(Settings.BrowserType)
        {

            InitializeSettings();
            //NaviateSite();
        }
        [BeforeFeature]
        public static void TestStart()
        {

            HookInitialize init = new HookInitialize();
        }
}
}
