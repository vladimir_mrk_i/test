﻿using System.Configuration;

namespace EuroMoneyConferences.ConfigElement
{
    public class EMCTestConfiguration: ConfigurationSection
    {
        private static EMCTestConfiguration _testConfig = (EMCTestConfiguration)ConfigurationManager.GetSection("EMCTestConfiguration");

        public static EMCTestConfiguration EMCSettings { get { return _testConfig; } }

        [ConfigurationProperty("testSettings")]
        public EMCElementCollection TestSettings { get { return (EMCElementCollection)base["testSettings"]; } }

    }
}
