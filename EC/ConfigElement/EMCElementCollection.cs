﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuroMoneyConferences.ConfigElement
{
    [ConfigurationCollection(typeof(EMCElement), AddItemName = "testSetting", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class EMCElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new EMCElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as EMCElement).Name;
        }

        public new EMCElement this[string type]
        {
            get
            {
                return (EMCElement)base.BaseGet(type);
            }
        }
    }
}
