﻿using EuroMoneyConferences.Base;
using EuroMoneyConferences.ConfigElement;
using System;

namespace EuroMoneyConferences.Config
{
    public class ConfigReader
    {

        //public static void SetFrameworkSettings()
        //{

        //    XPathItem aut;
        //    XPathItem testtype;
        //    XPathItem islog;
        //    XPathItem isreport;
        //    XPathItem buildname;
        //    XPathItem logPath;
        //    XPathItem appConnection;
        //    XPathItem browsertype;

        //    string strFilename = @"C:\Users\vladimirm\Documents\Visual Studio 2015\Projects\EC\EC\Config\GlobalConfig.xml";
        //    FileStream stream = new FileStream(strFilename, FileMode.Open);
        //    XPathDocument document = new XPathDocument(stream);
        //    XPathNavigator navigator = document.CreateNavigator();

        //    //Get XML Details and pass it in XPathItem type variables
        //    aut = navigator.SelectSingleNode("EAAutoFramework/RunSettings/AUT");
        //    buildname = navigator.SelectSingleNode("EAAutoFramework/RunSettings/BuildName");
        //    testtype = navigator.SelectSingleNode("EAAutoFramework/RunSettings/TestType");
        //    islog = navigator.SelectSingleNode("EAAutoFramework/RunSettings/IsLog");
        //    isreport = navigator.SelectSingleNode("EAAutoFramework/RunSettings/IsReport");
        //    logPath = navigator.SelectSingleNode("EAAutoFramework/RunSettings/LogPath");
        //    appConnection = navigator.SelectSingleNode("EAAutoFramework/RunSettings/ApplicationDb");
        //    browsertype = navigator.SelectSingleNode("EAAutoFramework/RunSettings/Browser");

        //    //Set XML Details in the property to be used accross framework
        //    Settings.AUT = aut.Value.ToString();
        //    Settings.BuildName = buildname.Value.ToString();
        //    Settings.TestType = testtype.Value.ToString();
        //    Settings.IsLog = islog.Value.ToString();
        //    Settings.IsReporting = isreport.Value.ToString();
        //    Settings.LogPath = logPath.Value.ToString();
        //    Settings.AppConnectionString = appConnection.Value.ToString();
        //    Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), browsertype.Value.ToString());
        //}


        public static void SetFrameworkSettings()
        {
            Settings.AUT = EMCTestConfiguration.EMCSettings.TestSettings["staging"].AUT;
            //Settings.BuildName = buildname.Value.ToString();
            Settings.TestType = EMCTestConfiguration.EMCSettings.TestSettings["staging"].TestType;
            Settings.IsLog = EMCTestConfiguration.EMCSettings.TestSettings["staging"].IsLog;
            //Settings.IsReporting = EATestConfiguration.EASettings.TestSettings["staging"].IsReadOnly;
            Settings.LogPath = EMCTestConfiguration.EMCSettings.TestSettings["staging"].LogPath;
            //Settings.AppConnectionString = appConnection.Value.ToString();
            Settings.BrowserType = (BrowserType)Enum.Parse(typeof(BrowserType), EMCTestConfiguration.EMCSettings.TestSettings["staging"].Browser);
        }

    }
}
