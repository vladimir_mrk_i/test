﻿using EuroMoneyConferences.Config;
using EuroMoneyConferences.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuroMoneyConferences.Base
{
    public abstract class BaseStep : Base
    {

        public virtual void NaviateSite()
        {
           DriverContext.Browser.GoToUrl(Settings.AUT);
           // DriverContext.Browser.GoToUrl("http://www.google.rs"); 
            LogHelpers.Write("Opened the browser !!!");
        }

    }
}
