﻿using EuroMoneyConferences.Helpers;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.IO;
using EuroMoneyConferences.Config;
using OpenQA.Selenium.Remote;
using System;

namespace EuroMoneyConferences.Base
{
   
        public abstract class TestInitializeHook 
        {

            public readonly BrowserType Browser;
            public TestInitializeHook(BrowserType browser)
            {

                Browser = browser;
            }
            public void InitializeSettings()
            {
                //Set all the settings for framework
                ConfigReader.SetFrameworkSettings();

                //Set Log
                LogHelpers.CreateLogFile();

                //Open Browser
                OpenBrowser(Settings.BrowserType);

                LogHelpers.Write("Initialized framework");

            }

            public void OpenBrowser(BrowserType browserType)
            {
                switch (browserType)
                {
                    case BrowserType.InternetExplorer:
                        DriverContext.Driver = new InternetExplorerDriver();
                        DriverContext.Browser = new Browser(DriverContext.Driver);
                        break;
                    case BrowserType.FireFox:
                        DriverContext.Driver = new FirefoxDriver();
                        DriverContext.Browser = new Browser(DriverContext.Driver);
                        break;
                    case BrowserType.Chrome:
                        DriverContext.Driver = new ChromeDriver(@"C:\chromedriver");
                        DriverContext.Browser = new Browser(DriverContext.Driver);
                        break;
                    case BrowserType.SauceLab:
                    DesiredCapabilities caps = new DesiredCapabilities();
                    caps.SetCapability(CapabilityType.BrowserName, "chrome");
                    caps.SetCapability(CapabilityType.Version, "");
                    caps.SetCapability(CapabilityType.Platform, "Windows 7");
                    caps.SetCapability("username", "vladimirm19841");
                    caps.SetCapability("accesskey", "6d1307e9-abcd-4036-bf82-f2c9856ecb1d");
                    DriverContext.Driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), caps);
                    DriverContext.Browser = new Browser(DriverContext.Driver);
                    break;
            }
            }

            public virtual void NaviateSite()
            {
                DriverContext.Browser.GoToUrl("http://www.google.rs");
                LogHelpers.Write("Opened the browser !!!");
            }



        }

}

